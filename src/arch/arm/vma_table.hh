/*
 * Copyright (c) 2016 Google Inc.
 * All rights reserved.
 *
 * The license below extends only to copyright in the software and
 * shall not be construed as granting a license to any other
 * intellectual property including but not limited to intellectual
 * property relating to a hardware implementation of the
 * functionality of the software licensed hereunder.  You may use the
 * software subject to the license terms below provided that you
 * ensure that this notice is replicated unmodified and in its
 * entirety in all distributions of the software, modified or
 * unmodified, in source code or in binary form.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Zach Yannes
 */

#ifndef __ARCH_ARM_VMA_TABLE_HH__
#define __ARCH_ARM_VMA_TABLE_HH__

#include <unordered_map>

#include "base/trace.hh"
#include "base/types.hh"

#include "sim/system.hh"
#include "debug/VMATrace.hh"
#include "base/addr_range_map.hh"
#include <iostream>

using namespace std;

class ThreadContext;
class VMATable;

typedef VMATable *VMATablePtr;

class VMAEntry {
private:
  uint32_t vm_area;
  uint32_t vm_start, vm_end;
  uint32_t vm_file, vm_dentry;
  std::string d_iname;
  uint64_t r_count;
  uint64_t w_count;

public:
  VMAEntry();
  VMAEntry(uint32_t vm_area, uint32_t vm_start, uint32_t vm_end,
    uint32_t vm_file, uint32_t vm_dentry, std::string d_iname,
    uint64_t r_count, uint64_t w_count);
  VMAEntry(ThreadContext *tc, uint32_t vaddr, bool isRead);

  std::string to_string() {
    std::ostringstream s;
    s << vm_area << ", " << vm_start << ", " << vm_end << ", " << vm_file
      << ", " << vm_dentry << ", " << d_iname << ", " << r_count << ", "
      << w_count;
    return s.str();
  }

  friend ostream& operator<<(ostream& os, const VMAEntry& vmaEntry);

  const uint32_t getVMArea() const {
    return vm_area;
  }

  const uint32_t getVMStart() const {
    return vm_start;
  }

  const uint32_t getVMEnd() const {
    return vm_end;
  }

  const uint32_t getVMFile() const {
    return vm_file;
  }

  const uint32_t getVMDentry() const {
    return vm_dentry;
  }

  const std::string getDIname() const {
    return d_iname;
  }

  const uint64_t getRCount() const {
    return r_count;
  }

  const uint64_t getWCount() const {
    return w_count;
  }

  void incrementReadCount() {
    r_count++;
  }

  void incrementWriteCount() {
    w_count++;
  }
};

typedef VMAEntry *VMAEntryPtr;

class VMATable {
public:
  AddrRangeMap<VMAEntryPtr> table;
  VMATable();

  VMAEntry* UpdateVMAEntry(ThreadContext *tc, uint32_t addr, bool isRead);

  std::string to_string() {
    std::ostringstream s;
    // TODO: Print VMATable stats
    for(auto it = table.begin(); it != table.end(); ++it) {
      s << it->second << "\n";
    }
    return s.str();
  }

  friend ostream& operator<<(ostream& os, const VMATable& vmaTable);

};

#endif
