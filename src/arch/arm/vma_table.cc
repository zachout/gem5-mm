/*
 * Copyright (c) 2016 Google Inc.
 * All rights reserved.
 *
 * The license below extends only to copyright in the software and
 * shall not be construed as granting a license to any other
 * intellectual property including but not limited to intellectual
 * property relating to a hardware implementation of the
 * functionality of the software licensed hereunder.  You may use the
 * software subject to the license terms below provided that you
 * ensure that this notice is replicated unmodified and in its
 * entirety in all distributions of the software, modified or
 * unmodified, in source code or in binary form.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Rahul Thakur
 */

#include "arch/arm/vma_table.hh"
#include "arch/arm/linux/threadinfo.hh"
#include "cpu/thread_context.hh"

// using namespace ArmISA;
using namespace ArmLinux;

VMAEntry::VMAEntry() :
  vm_area(0),
  vm_start(0),
  vm_end(0),
  vm_file(0),
  vm_dentry(0),
  d_iname("anonymous"),
  r_count(0), w_count(0) {}

VMAEntry::VMAEntry(uint32_t vm_area, uint32_t vm_start, uint32_t vm_end,
  uint32_t vm_file, uint32_t vm_dentry, std::string d_iname,
  uint64_t r_count, uint64_t w_count) :
  vm_area(vm_area),
  vm_start(vm_start),
  vm_end(vm_end),
  vm_file(vm_file),
  vm_dentry(vm_dentry),
  d_iname(d_iname),
  r_count(r_count), w_count(w_count) {}

VMAEntry::VMAEntry(ThreadContext *tc, uint32_t vaddr, bool isRead) {
  ArmLinux::ThreadInfo ti(tc);

  Addr task_descriptor = tc->readIntReg(2);
  // task_descriptor = tc->readIntReg(ArmISA::INTREG_SP_SVC);

  uint32_t pid = ti.curTaskPID(task_descriptor);
  uint32_t tgid = ti.curTaskTGID(task_descriptor);
  std::string next_task_str = ti.curTaskName(task_descriptor);

  int32_t mm = ti.curTaskMm(task_descriptor);
  if(mm >= 0) {
    vm_area  = ti.curTaskVmArea(mm);
  }

  while(vm_area > 0) {
    vm_start = -1, vm_end = -1;
    vm_file = -1, vm_dentry = -1;

    vm_start = ti.curTaskVmStart(vm_area);
    vm_end = ti.curTaskVmEnd(vm_area);
    vm_file = ti.curTaskVmFile(vm_area);
    if(vm_file > 0) {
      vm_dentry = ti.curTaskDentry(vm_file);
    }
    if(vm_dentry > 0) {
      d_iname = ti.curTaskDiname(vm_dentry);
    }

    if(vaddr >= vm_start && vaddr <= vm_end) {
      uint32_t r_count = isRead ? 1 : 0;
      uint32_t w_count = isRead ? 0 : 1;
      break;
    }
    vm_area = ti.curTaskVmNext(vm_area);
  }
}


ostream& operator<<(ostream& os, const VMAEntry& vmaEntry) {
  os << vmaEntry.getVMArea() << ", " << vmaEntry.getVMStart() << ", "
    << vmaEntry.getVMEnd() << ", " << vmaEntry.getVMFile()
    << ", " << vmaEntry.getVMDentry() << ", " << vmaEntry.getDIname()
    << ", " << vmaEntry.getRCount() << ", " << vmaEntry.getWCount() << "\n";
}

VMATable::VMATable() {

}

VMAEntry* VMATable::UpdateVMAEntry(ThreadContext *tc, uint32_t vaddr, bool isRead) {
  auto it = table.find(vaddr);
  if(it != table.end()) {
    if(isRead)
      it->second->incrementReadCount();
    else
      it->second->incrementWriteCount();
    return it->second;
  } else {
    // Lookup VMAEntry from tc
    VMAEntry *vmaEntry = new VMAEntry(tc, vaddr, isRead);
    table.insert(AddrRange(vmaEntry->getVMStart(), vmaEntry->getVMEnd()), vmaEntry);
    return vmaEntry;
  }
}

ostream& operator<<(ostream& os, const VMATable& vmaTable) {
  for(auto it = vmaTable.table.begin(); it != vmaTable.table.end(); ++it) {
    os << *(it->second);
  }
}
