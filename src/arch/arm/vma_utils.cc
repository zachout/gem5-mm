/*
 * Copyright (c) 2016 Google Inc.
 * All rights reserved.
 *
 * The license below extends only to copyright in the software and
 * shall not be construed as granting a license to any other
 * intellectual property including but not limited to intellectual
 * property relating to a hardware implementation of the
 * functionality of the software licensed hereunder.  You may use the
 * software subject to the license terms below provided that you
 * ensure that this notice is replicated unmodified and in its
 * entirety in all distributions of the software, modified or
 * unmodified, in source code or in binary form.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Rahul Thakur
 */

#include "arch/arm/vma_utils.hh"
#include "base/output.hh"
#include "debug/VMAUtil.hh"

namespace ArmISA {

  AddrRangeMap<VMAPtr>
  taskVMA(ThreadContext *tc)
  {
    AddrRangeMap<VMAPtr> vmaMap;
    int32_t mm = 0;
    uint32_t vm_area = 0;
    std::string d_iname = "anonymous";

    ArmLinux::ThreadInfo ti(tc);
    Addr task_descriptor = task_descriptor = tc->readIntReg(2);
    // task_descriptor = tc->readIntReg(ArmISA::INTREG_SP_SVC);

    uint32_t pid = ti.curTaskPID(task_descriptor);
    uint32_t tgid = ti.curTaskTGID(task_descriptor);
    std::string next_task_str = ti.curTaskName(task_descriptor);
    
    mm = ti.curTaskMm(task_descriptor);
    ccprintf(std::cerr, "TaskVMA: pid %d, tgid %d, task %s, mm %d\n",
	     pid, tgid, next_task_str, mm);
    if(mm >= 0) {
      vm_area  = ti.curTaskVmArea(mm);
      ccprintf(std::cerr, "Looked up vm_area: %llx\n", vm_area);
    }
    
    while(vm_area > 0) {
      int32_t vm_start = 0, vm_end = 0;
      uint32_t vm_file = 0, vm_dentry = 0;
      
      vm_start = ti.curTaskVmStart(vm_area);
      vm_end = ti.curTaskVmEnd(vm_area);
      vm_file = ti.curTaskVmFile(vm_area);
      if(vm_file > 0) {
	vm_dentry = ti.curTaskDentry(vm_file);
      }
      if(vm_dentry > 0) {
	d_iname = ti.curTaskDiname(vm_dentry);
      }
      vm_area = ti.curTaskVmNext(vm_area);

      VMATask vma(vm_area, vm_start, vm_end,
		  vm_file, vm_dentry, d_iname);
      vmaMap.insert(AddrRange(vm_start, vm_end), &vma);
      
      DPRINTF(VMAUtil, "taskVMA: addr %d: %d, [%d - %d], %d, %d, %s\n",
	      mm, vm_area, vm_start, vm_end, vm_file, vm_dentry, d_iname);
    }
    return vmaMap;
  }
    
    
std::string
lookupTaskName(ThreadContext *tc)
{
    ArmLinux::ThreadInfo ti(tc);
    Addr task_descriptor = 0;
    std::string next_task_str = "unknown";

    task_descriptor = tc->readIntReg(2);
    if(myvtophys(tc, task_descriptor) > 0) {
        // pid = ti.curTaskPID(task_descriptor);        
        // tgid = ti.curTaskTGID(task_descriptor);
        next_task_str = ti.curTaskName(task_descriptor);
        // mm = ti.curTaskMm(task_descriptor);
    }

    return next_task_str;
}
    
int
lookupType(ThreadContext *tc, Addr addr)
{
    int type = LOOKUP_OTHER;
    System *system = tc->getSystemPtr();

    if((system != NULL) &&
       (addr >= system->getKernelStart() && addr <= system->getKernelEnd())) {
        type = LOOKUP_KERNEL;
    } else if(FullSystem && ArmISA::isValidProcess(tc)) {
        type = LOOKUP_STACK;
    } else {
        type = LOOKUP_OTHER;
    }

    return type;
}

const char*
lookupVMA(ThreadContext *tc, Addr addr)
{
    Addr symaddr, nextaddr;
    std::string symbol = "unknown", symbol2;
    int pid = -1;
    ArmISA::ProcessInfo* procInfo;
    int ltype; 
    System *system;
    Process *proc;

    ltype = lookupType(tc, addr);
    system = tc->getSystemPtr();
    proc = tc->getProcessPtr();
    if(proc != NULL) {
        DPRINTF(VMAUtil, "lookupVMA: process %s\n", proc->name());
    }
    switch(ltype) {
    case LOOKUP_KERNEL:
        if(system != NULL) {
            system->kernelSymtab->findNearestSymbol(addr,
                                                    symbol, symaddr, nextaddr);
        }
        DPRINTF(VMAUtil, "lookupKernelAddress addr %llx: %s, %x, %x\n",
                addr, symbol, symaddr, nextaddr);
        break;
    case LOOKUP_STACK:
        symaddr = tc->readIntReg(ArmISA::INTREG_SP_SVC); // getStackAddr(tc);
        procInfo = new ArmISA::ProcessInfo(tc);
        symbol = procInfo->safeName(symaddr);
        pid = procInfo->safePid(symaddr);

        symbol2 = lookupTaskName(tc);
        if(symbol == symbol2) {
            DPRINTF(VMAUtil, "lookupStackAddress %llx: %llx %s, pid %d\n",
                    addr, symaddr, symbol, pid);
        } else {
            DPRINTF(VMAUtil, "lookupStackAddress %llx: %llx %s (%s), pid %d\n",
                    addr, symaddr, symbol, symbol2, pid);
        }
        // delete procInfo;
        
        break;
    case LOOKUP_OTHER:
    default:
        symbol = "other";
        break;
    }

    return symbol.c_str();
}

} // namespace ArmISA
