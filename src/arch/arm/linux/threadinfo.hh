/*
 * Copyright (c) 2004 The Regents of The University of Michigan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Ali Saidi
 *          Nathan Binkert
 *          Dam Sunwoo
 */

#ifndef __ARCH_ARM_LINUX_THREADINFO_HH__
#define __ARCH_ARM_LINUX_THREADINFO_HH__

#include "cpu/thread_context.hh"
#include "sim/system.hh"
#include "sim/vptr.hh"
// #include "arch/arm/vma_utils.hh"
#include "arch/arm/vtophys.hh"
#include "debug/VMAUtil.hh"
#include "base/loader/symtab.hh"

namespace ArmISA {
    Addr myvtophys(ThreadContext *tc, Addr vaddr);
}

namespace ArmLinux {

    // using namespace ArmISA;
    
class ThreadInfo
{
  private:
    ThreadContext *tc;
    System *sys;
    Addr pcbb;

    template <typename T>
    bool
    get_data(const char *symbol, T &data)
    {
        Addr addr = 0;
        if (!sys->kernelSymtab->findAddress(symbol, addr)) {
            warn_once("Unable to find kernel symbol %s\n", symbol);
            warn_once("Kernel not compiled with task_struct info; can't get "
                      "currently executing task/process/thread name/ids!\n");
            return false;
        }

        if(!ArmISA::virtvalid(tc, addr)) {
            return false;
        }
        
        CopyOut(tc, &data, addr, sizeof(T));

        data = TheISA::gtoh(data);

        return true;
    }

    template <typename T>
    bool loadData(const char *symbol, T base, T&offset)
    {
        if (!get_data(symbol, offset))
            return false;

        if(ArmISA::virtvalid(tc, base + offset)) {
            return true;
        }
        return false;
    }

  public:
    ThreadInfo(ThreadContext *_tc, Addr _pcbb = 0)
        : tc(_tc), sys(tc->getSystemPtr()), pcbb(_pcbb)
    {

    }
    ~ThreadInfo()
    {}

    inline Addr
    curThreadInfo()
    {
        /*
        if (!TheISA::CurThreadInfoImplemented)
            panic("curThreadInfo() not implemented for this ISA");
        */
        Addr addr = pcbb;
        Addr sp;
        /*
        if (!addr)
            addr = tc->readMiscRegNoEffect(TheISA::CurThreadInfoReg);
        */
        if (!addr)
            addr = tc->readIntReg(ArmISA::INTREG_SP_SVC); // INTREG_SP);
        PortProxy &p = tc->getPhysProxy();
        p.readBlob(addr, (uint8_t *)&sp, sizeof(Addr));

        return sp & ~ULL(0x3fff);
    }

    inline Addr
    curTaskInfo(Addr thread_info = 0)
    {
        int32_t offset;
        if (!get_data("thread_info_task", offset))
            return 0;

        if (!thread_info)
            thread_info = curThreadInfo();

        Addr addr;
        if(ArmISA::virtvalid(tc, thread_info + offset)) {
            CopyOut(tc, &addr, thread_info + offset, sizeof(addr));
        }        
        // CopyOut(tc, &addr, thread_info + offset, sizeof(addr));

        return addr;
    }

    int32_t
    curTaskPID(Addr thread_info = 0)
    {
        int32_t offset;
        if (!get_data("task_struct_pid", offset))
            return -1;

        int32_t pid;
        CopyOut(tc, &pid, curTaskInfo(thread_info) + offset, sizeof(pid));

        return pid;
    }
  
  int32_t
  curTaskTGID(Addr thread_info = 0)
  {
    int32_t offset;
    if (!get_data("task_struct_tgid", offset))
      return -1;

    int32_t tgid;
    CopyOut(tc, &tgid, curTaskInfo(thread_info) + offset, sizeof(tgid));

    return tgid;
  }

  int64_t
  curTaskStart(Addr thread_info = 0)
  {
    int32_t offset;
    if (!get_data("task_struct_start_time", offset))
      return -1;

    int64_t data;
    // start_time is actually of type timespec, but if we just
    // grab the first long, we'll get the seconds out of it
    CopyOut(tc, &data, curTaskInfo(thread_info) + offset, sizeof(data));

    return data;
  }

  std::string
  curTaskName(Addr thread_info = 0)
  {
    int32_t offset;
    int32_t size;

    if (!get_data("task_struct_comm", offset))
      return "FailureIn_curTaskName";

    if (!get_data("task_struct_comm_size", size))
      return "FailureIn_curTaskName";

    char buffer[size + 1];
    Addr vaddr = curTaskInfo(thread_info);
    if(ArmISA::virtvalid(tc, vaddr + offset)) {
      CopyStringOut(tc, buffer, vaddr + offset, size);
    }
    DPRINTF(VMAUtil, "%s: thread_info %llx, vaddr %llx, offset %d, size %d\n",
	    __func__, thread_info, vaddr, offset, size);

    return buffer;
  }

  int32_t
  curTaskMm(Addr thread_info = 0)
  {
    int32_t offset;
    if (!get_data("task_struct_mm", offset))
      return -1;

    int32_t mm_ptr;
    Addr addr = curTaskInfo(thread_info);
    if(ArmISA::virtvalid(tc, addr + offset)) {
      CopyOut(tc, &mm_ptr, addr + offset, sizeof(mm_ptr));
    }
    return mm_ptr;
  }

  int32_t
  curTaskActiveMm(Addr thread_info = 0)
  {
    int32_t offset;
    if (!get_data("task_struct_active_mm", offset))
      return -1;

    int32_t mm_ptr;
    Addr addr = curTaskInfo(thread_info);
    if(ArmISA::virtvalid(tc, addr + offset)) {
      CopyOut(tc, &mm_ptr, addr + offset, sizeof(mm_ptr));
    }
    return mm_ptr;
  }
    
  int32_t
  curTaskVmArea(Addr mm_info)
  {
    int32_t offset;
    if(!get_data("mm_struct_mmap", offset))
      return -1;

    int32_t vm_area;
        
    if(ArmISA::virtvalid(tc, mm_info + offset)) {
      CopyOut(tc, &vm_area, mm_info + offset, sizeof(vm_area));
    }
    return vm_area;
  }

  Addr
  curTaskVmStart(Addr vm_area)
  {
    int32_t offset;
    if(!get_data("vm_area_struct_vm_start", offset))
      return -1;

    Addr vm_start;
    if(ArmISA::virtvalid(tc, vm_area + offset)) {
      CopyOut(tc, &vm_start, vm_area + offset, sizeof(vm_start));
    }

    return vm_start;
  }

  Addr
  curTaskVmEnd(Addr vm_area)
  {
    int32_t offset;
    if(!get_data("vm_area_struct_vm_end", offset))
      return -1;

    Addr vm_end;
    if(ArmISA::virtvalid(tc, vm_area + offset)) {
      CopyOut(tc, &vm_end, vm_area + offset, sizeof(vm_end));
    }

    return vm_end;
  }

  int32_t
  curTaskVmNext(Addr vm_area)
  {
    int32_t offset;
    if(!get_data("vm_area_struct_vm_next", offset))
      return -1;

    int32_t vm_next;
    if(ArmISA::virtvalid(tc, vm_area + offset)) {
      CopyOut(tc, &vm_next, vm_area + offset, sizeof(vm_next));
    }
    return vm_next;
  }

  int32_t
  curTaskVmMM(Addr vm_area)
  {
    int32_t offset;
    if(!get_data("vm_area_struct_vm_mm", offset))
      return -1;

    int32_t vm_mm;
    if(ArmISA::virtvalid(tc, vm_area + offset)) {
      CopyOut(tc, &vm_mm, vm_area + offset, sizeof(vm_mm));
    }
    return vm_mm;
  }
  
  int32_t
  curTaskVmFile(Addr vm_area)
  {
    int32_t offset;
    if(!get_data("vm_area_struct_vm_file", offset))
      return 0;

    int32_t vm_file;
    if(ArmISA::virtvalid(tc, vm_area + offset)) {
      CopyOut(tc, &vm_file, vm_area + offset, sizeof(vm_file));
    }

    return vm_file;
  }

  uint32_t
  curTaskDentry(Addr vm_file)
  {
    int32_t f_path_offset;
    int32_t offset;

    if(!get_data("file_struct_f_path", f_path_offset))
      return 0;
    if(!get_data("path_dentry", offset))
      return 0;

    uint32_t dentry;
    if(ArmISA::virtvalid(tc, vm_file + f_path_offset + offset)) {
      CopyOut(tc, &dentry, vm_file + f_path_offset + offset, sizeof(dentry));
    }
    return dentry;
  }

  std::string
  curTaskDiname(Addr dentry)
  {
    int32_t offset;
    int32_t size = 40;

    if (!get_data("dentry_d_iname", offset))
      return "FailureIn_curTaskDiname";

    char buffer[size + 1];
    if(ArmISA::virtvalid(tc, dentry + offset)) {
      CopyStringOut(tc, buffer, dentry + offset, size);
    }
    return std::string(buffer);
  }
    
  uint32_t
  curTaskStartStack(Addr mm_info)
  {
    int32_t offset;
    if (!get_data("mm_struct_start_stack", offset))
      return -1;

    uint32_t start_stack;
    if(ArmISA::virtvalid(tc, mm_info + offset)) {
      CopyOut(tc, &start_stack, mm_info + offset, sizeof(start_stack));
    }
    return start_stack;
  }

  int32_t
  vmFaultVma(Addr vm_fault)
  {
    int32_t offset;
    if(!get_data("vm_fault_vma", offset))
      return 0;

    int32_t vma;
    if(ArmISA::virtvalid(tc, vm_fault + offset)) {
      CopyOut(tc, &vma, vm_fault + offset, sizeof(vma));
    }

    return vma;
  }

  unsigned long
  vmFaultAddr(Addr vm_fault)
  {
    int32_t offset;
    if(!get_data("vm_fault_address", offset))
      return 0;

    unsigned long addr;
    if(ArmISA::virtvalid(tc, vm_fault + offset)) {
      CopyOut(tc, &addr, vm_fault + offset, sizeof(addr));
    }

    return addr;
  }

  
  uint32_t
  vmRegionStart(Addr vm_region)
  {
    int32_t offset;
    if (!get_data("vm_region_vm_start", offset))
      return -1;

    uint32_t vm_start;
    if(ArmISA::virtvalid(tc, vm_region + offset)) {
      CopyOut(tc, &vm_start, vm_region + offset, sizeof(vm_start));
    }
    return vm_start;
  }

  uint32_t
  vmRegionEnd(Addr vm_region)
  {
    int32_t offset;
    if (!get_data("vm_region_vm_end", offset))
      return -1;

    uint32_t vm_end;
    if(ArmISA::virtvalid(tc, vm_region + offset)) {
      CopyOut(tc, &vm_end, vm_region + offset, sizeof(vm_end));
    }
    return vm_end;
  }

  uint32_t
  vmRegionUsage(Addr vm_region)
  {
    int32_t offset;
    if (!get_data("vm_region_vm_usage", offset))
      return -1;

    uint32_t vm_usage;
    if(ArmISA::virtvalid(tc, vm_region + offset)) {
      CopyOut(tc, &vm_usage, vm_region + offset, sizeof(vm_usage));
    }
    return vm_usage;
  }

  uint32_t
  vmRegionFile(Addr vm_region)
  {
    int32_t offset;
    if (!get_data("vm_region_vm_file", offset))
      return -1;

    uint32_t vm_file;
    if(ArmISA::virtvalid(tc, vm_region + offset)) {
      CopyOut(tc, &vm_file, vm_region + offset, sizeof(vm_file));
    }
    return vm_file;
  }  
  
  
  Addr
  readStackPointer()
  {
    return tc->readIntReg(ArmISA::INTREG_SP_SVC);
  }
};

} // namespace ArmLinux

#endif // __ARCH_ARM_LINUX_THREADINFO_HH__
