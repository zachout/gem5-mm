/*
 * Copyright (c) 2016 Google Inc.
 * All rights reserved.
 *
 * The license below extends only to copyright in the software and
 * shall not be construed as granting a license to any other
 * intellectual property including but not limited to intellectual
 * property relating to a hardware implementation of the
 * functionality of the software licensed hereunder.  You may use the
 * software subject to the license terms below provided that you
 * ensure that this notice is replicated unmodified and in its
 * entirety in all distributions of the software, modified or
 * unmodified, in source code or in binary form.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Rahul Thakur
 */

#ifndef __SIM_PROBE_VMA_HH__
#define __SIM_PROBE_VMA_HH__

#include <memory>

#include "mem/packet.hh"
#include "arch/arm/vmatask.hh"
#include "sim/probe/probe.hh"

namespace ProbePoints {
  struct VMAPacketInfo {
    MemCmd cmd;
    Addr addr;
    uint32_t size;
    bool isRead;
    ContextID contextId;

    explicit VMAPacketInfo(const PacketPtr& pkt) :
        cmd(pkt->cmd),
        addr(pkt->getAddr()),
        size(pkt->getSize()),
        isRead(pkt->isRead()),
        contextId(pkt->req->hasContextId() ?
                  pkt->req->contextId() : InvalidContextID) {}

        // contextId(pkt->req->threadId()) {}
  };

  typedef ProbePointArg<VMAPacketInfo> VMAPacket;
  typedef std::unique_ptr<VMAPacket> VMAPacketUPtr;

  struct VMAInfo {
    uint32_t vm_area;
    uint32_t vm_start, vm_end;
    uint32_t vm_file, vm_dentry;
    std::string d_iname;

    explicit VMAInfo() :
      vm_area(0),
      vm_start(0),
      vm_end(0),
      vm_file(0),
      vm_dentry(0),
      d_iname("") {}


    explicit VMAInfo(uint32_t vmarea,
		     uint32_t vmstart, uint32_t vmend,
		     uint32_t vmfile, uint32_t vmdentry,
		     std::string diname) :
      vm_area(vmarea),
      vm_start(vmstart),
      vm_end(vmend),
      vm_file(vmfile),
      vm_dentry(vmdentry),
      d_iname(diname) {}

    explicit VMAInfo(VMAPtr vma) :
      vm_area(vma->vm_area),
      vm_start(vma->vm_start),
      vm_end(vma->vm_end),
      vm_file(vma->vm_file),
      vm_dentry(vma->vm_dentry),
      d_iname(vma->d_iname) {}


    std::string to_string() const {
      return csprintf("VMA area %llx: [%llx - %llx], file %llx, dentry %llx, %s\n",
		      vm_area, vm_start, vm_end, vm_file, vm_dentry, d_iname);
    }
  };

  typedef ProbePointArg<VMAInfo> VMA;
  typedef std::unique_ptr<VMA> VMAUPtr;
}
#endif
