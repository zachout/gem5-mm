/*
 * Copyright (c) 2016 Google Inc.
 * All rights reserved.
 *
 * The license below extends only to copyright in the software and
 * shall not be construed as granting a license to any other
 * intellectual property including but not limited to intellectual
 * property relating to a hardware implementation of the
 * functionality of the software licensed hereunder.  You may use the
 * software subject to the license terms below provided that you
 * ensure that this notice is replicated unmodified and in its
 * entirety in all distributions of the software, modified or
 * unmodified, in source code or in binary form.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Rahul Thakur
 */

#include "mem/probes/vma_trace.hh"

#include "base/intmath.hh"
#include "params/VMATraceProbe.hh"
#include "debug/VMATrace.hh"
#include "base/output.hh"

VMALookup VMATraceProbe::createVMALookup(const ProbePoints::PacketInfo &pi)
{
    ThreadContext *tc = getThreadContext(pi.addr);

    VMALookup vma;
    vma.addr = pi.addr;
    
    if(tc == NULL) {
        vma.ltype = LOOKUP_OTHER;
        vma.name = "other";
    } else {
        vma.ltype = lookupType(tc, vma.addr);
        vma.name = lookupVMA(tc, vma.ltype, vma.addr);
    }

    return vma;
}

ThreadContext* VMATraceProbe::getThreadContext(Addr instaddr) {
    for(int i = 0; i < system->numContexts(); i++) {
        ThreadContext *tc = system->getThreadContext(i);
        if((tc != NULL) && (tc->instAddr() == instaddr)) {
            ccprintf(std::cerr, "Found thread context %d with addr 0x%llx\n",
                     i, instaddr);
            return system->getThreadContext(i);
        }
    }

    return NULL;
}

int VMATraceProbe::lookupType(ThreadContext *tc, Addr addr)
{
    int type = LOOKUP_OTHER;

    if(addr >= system->getKernelStart() && addr <= system->getKernelEnd()) {
        type = LOOKUP_KERNEL;
    } else if(FullSystem && ArmISA::isValidProcess(tc)) {
        type = LOOKUP_STACK;
    } else {
        type = LOOKUP_OTHER;
    }

    return type;
}

const char* VMATraceProbe::lookupVMA(ThreadContext *tc, int ltype,
                                     Addr addr)
{
    Addr symaddr, nextaddr;
    std::string symbol;
    int pid = -1;
    ArmISA::ProcessInfo* procInfo;

    switch(ltype) {
    case LOOKUP_KERNEL:
        system->kernelSymtab->findNearestSymbol(addr,
                                                symbol, symaddr, nextaddr);
        DPRINTF(VMATrace, "lookupKernelAddress addr %llx: %s, %x, %x\n",
                addr, symbol, symaddr, nextaddr);
        break;
    case LOOKUP_STACK:
        symaddr = getStackAddr(tc);
        procInfo = new ArmISA::ProcessInfo(tc);
        symbol = procInfo->name(symaddr);
        pid = procInfo->pid(symaddr);
        DPRINTF(VMATrace, "lookupStackAddress %llx: %llx %s, pid %d\n",
                addr, symaddr, symbol, pid);
        delete procInfo;
        
        break;
    case LOOKUP_OTHER:
    default:
        symbol = "other";
        break;
    }

    return symbol.c_str();
}

VMATraceProbe::VMATraceProbe(VMATraceProbeParams *p)
    : BaseMemProbe(p),
      traceStream(nullptr),
      cacheLineSizeLg2(floorLog2(p->system->cacheLineSize())),
      pageSizeLg2(floorLog2(p->page_size)),
      totalCacheLinesInMem(p->system->memSize() / p->system->cacheLineSize()),
      totalPagesInMem(p->system->memSize() / p->page_size),
      vmas(),
      system(p->system)
{
    std::string filename;

    if (p->trace_file != "") {
        filename = simout.resolve(p->trace_file);

        const std::string suffix = ".gz";
        if (p->trace_compress &&
            filename.compare(filename.size() - suffix.size(), suffix.size(),
                             suffix) != 0)
            filename = filename + suffix;
    } else {
        filename = simout.resolve(name() + ".trc" +
                                  (p->trace_compress ? ".gz" : ""));
    }

    traceStream = new ProtoOutputStream(filename);

    ProtoMessage::VMATraceHeader header_msg;
    header_msg.set_obj_id(name());
    header_msg.set_tick_freq(SimClock::Frequency);
    traceStream->write(header_msg);

    registerExitCallback(
        new MakeCallback<VMATraceProbe, &VMATraceProbe::closeStreams>(this));
    
    fatal_if(!isPowerOf2(system->cacheLineSize()),
             "VMATraceProbe expects cache line size is power of 2.");
    fatal_if(!isPowerOf2(p->page_size),
             "VMATraceProbe expects page size parameter is power of 2");
}

void
VMATraceProbe::closeStreams()
{
    if (traceStream != NULL)
        delete traceStream;
}

void
VMATraceProbe::regStats()
{
    BaseMemProbe::regStats();

    using namespace Stats;
    // clang-format off
    fpVMATotal.name(name() + ".vmas")
        .desc("VMAs at cache line granularity")
        .flags(nozero | nonan);

    registerResetCallback(
        new MakeCallback<VMATraceProbe, &VMATraceProbe::statReset>(
            this));
}

void
VMATraceProbe::insertVMA(VMALookup vma, VMASet *set)
{
    set->insert(vma);
}

void
VMATraceProbe::handleRequest(const ProbePoints::PacketInfo &pkt_info)
{
    if (!pkt_info.cmd.isRequest() || !system->isMemAddr(pkt_info.addr))
        return;

    VMALookup vma = VMATraceProbe::createVMALookup(pkt_info);
    
    ProtoMessage::VMATrace vma_msg;    
    vma_msg.set_name(vma.name);
    vma_msg.set_type(vma.ltype);
    vma_msg.set_tick(curTick());
    vma_msg.set_cmd(pkt_info.cmd.toInt());
    vma_msg.set_flags(pkt_info.flags);
    vma_msg.set_addr(pkt_info.addr);
    vma_msg.set_size(pkt_info.size);
    vma_msg.set_pc(pkt_info.pc);
    vma_msg.set_addr(vma.addr);

    insertVMA(vma, &vmas);
    fpVMATotal = vmas.size();
    
    traceStream->write(vma_msg);
}

void
VMATraceProbe::statReset()
{
    vmas.clear();
}

VMATraceProbe *
VMATraceProbeParams::create()
{
    return new VMATraceProbe(this);
}
