/*
 * Copyright (c) 2016 Google Inc.
 * All rights reserved.
 *
 * The license below extends only to copyright in the software and
 * shall not be construed as granting a license to any other
 * intellectual property including but not limited to intellectual
 * property relating to a hardware implementation of the
 * functionality of the software licensed hereunder.  You may use the
 * software subject to the license terms below provided that you
 * ensure that this notice is replicated unmodified and in its
 * entirety in all distributions of the software, modified or
 * unmodified, in source code or in binary form.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Rahul Thakur
 */

#ifndef __MEM_PROBES_VMA_TRACE_HH__
#define __MEM_PROBES_VMA_TRACE_HH__

#include <unordered_map>

#include "base/callback.hh"
#include "mem/packet.hh"
#include "mem/probes/base.hh"
#include "proto/protoio.hh"

#include "sim/stats.hh"
#include "sim/system.hh"
#include "proto/vma.pb.h"
#include "debug/VMATrace.hh"
#include "sim/full_system.hh"
#include "sim/process.hh"
#include "arch/arm/vtophys.hh"
#include "arch/arm/stacktrace.hh"

struct VMATraceProbeParams;

const static int LOOKUP_OTHER = 0;
const static int LOOKUP_KERNEL = 1;
const static int LOOKUP_STACK = 2;

typedef struct vma_lookup {
    const char *name;
    int ltype;
    Addr addr;        
} VMALookup;

inline bool operator== (const VMALookup &lhs, const VMALookup &rhs)
{
    return (lhs.name == rhs.name) &&
        (lhs.ltype == rhs.ltype) &&
        (lhs.addr == rhs.addr);
}

struct Hash {
    size_t operator()(const VMALookup &vma) const {
        std::string temp = vma.name
            + std::to_string(vma.ltype)
            + std::to_string(vma.addr);
        return (temp.length());
    }
};
typedef std::unordered_set<VMALookup, Hash> VMASet;

/// Probe to track VMAs of accessed memory
class VMATraceProbe : public BaseMemProbe
{
  public:    
    VMALookup createVMALookup(const ProbePoints::PacketInfo &pi);

    ThreadContext* getThreadContext(Addr instaddr);
    int lookupType(ThreadContext *tc, Addr addr);
    static Addr getStackAddr(ThreadContext *tc) { return tc->readIntReg(INTREG_SP); }
    const char* lookupVMA(ThreadContext *tc, int ltype, Addr addr);
            
    VMATraceProbe(VMATraceProbeParams *p);
    void closeStreams();

    void regStats() override;

    void statReset();

  protected:
    /** Trace output stream */
    ProtoOutputStream *traceStream;

    /// Cache Line size for footprint measurement (log2)
    const uint8_t cacheLineSizeLg2;
    /// Page size for footprint measurement (log2)
    const uint8_t pageSizeLg2;
    const uint64_t totalCacheLinesInMem;
    const uint64_t totalPagesInMem;

    void handleRequest(const ProbePoints::PacketInfo &pkt_info) override;

    void insertVMA(VMALookup vma, VMASet *sets);
    
    /// Footprint at page granularity, since simulation begin
    Stats::Scalar fpVMATotal;
    
    // VMA set to track 
    VMASet vmas;
    System *system;
};

#endif  //__MEM_PROBES_MEM_FOOTPRINT_HH__
