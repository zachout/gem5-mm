/*
 * Copyright (c) 2015 ARM Limited
 * All rights reserved
 *
 * The license below extends only to copyright in the software and shall
 * not be construed as granting a license to any other intellectual
 * property including but not limited to intellectual property relating
 * to a hardware implementation of the functionality of the software
 * licensed hereunder.  You may use the software subject to the license
 * terms below provided that you ensure that this notice is replicated
 * unmodified and in its entirety in all distributions of the software,
 * modified or unmodified, in source code or in binary form.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Andreas Hansson
 *          Andreas Sandberg
 */

#include "mem/probes/vma_table_trace.hh"

#include "base/callback.hh"
#include "base/output.hh"
#include "params/VMATableProbe.hh"
#include "proto/vma.pb.h"
#include <iostream>

struct VMATableProbe;

VMATableProbe::VMATableProbe(VMATableProbeParams *p)
    : BaseMemProbe(p),
      system(p->system),
      traceStream(nullptr),
      stats_file(p->vma_stats_file)
{

  std::string filename;
  if (p->trace_file != "") {
      // If the trace file is not specified as an absolute path,
      // append the current simulation output directory
      filename = simout.resolve(p->trace_file);

      const std::string suffix = ".gz";
      // If trace_compress has been set, check the suffix. Append
      // accordingly.
      if (p->trace_compress &&
          filename.compare(filename.size() - suffix.size(), suffix.size(),
                           suffix) != 0)
          filename = filename + suffix;
  } else {
      // Generate a filename from the name of the SimObject. Append .trc
      // and .gz if we want compression enabled.
      filename = simout.resolve(name() + ".trc" +
                                (p->trace_compress ? ".gz" : ""));
  }

  traceStream = new ProtoOutputStream(filename);

  // Create a protobuf message for the header and write it to
  // the stream
  ProtoMessage::VMAHeader header_msg;
  header_msg.set_obj_id(name());
  header_msg.set_tick_freq(SimClock::Frequency);
  traceStream->write(header_msg);
    // Register a callback to compensate for the destructor not
    // being called. The callback forces the stream to flush and
    // closes the output file.
    registerExitCallback(
        new MakeCallback<VMATableProbe, &VMATableProbe::closeStreams>(this));
}

void
VMATableProbe::regProbeListeners()
{
    const VMATableProbeParams *p(
        dynamic_cast<const VMATableProbeParams *>(params()));
    assert(p);

    vmalisteners.resize(p->manager.size());
    for (int i = 0; i < p->manager.size(); i++) {
        ProbeManager *const mgr(p->manager[i]->getProbeManager());
        vmalisteners[i].reset(new VMAPacketListener(*this, mgr, p->probe_name));
    }
}

void
VMATableProbe::closeStreams()
{
  std::ofstream outfile;
  outfile.open(stats_file);
  outfile << table;
  outfile.close();

  if (traceStream != NULL)
      delete traceStream;
}

void
VMATableProbe::handleRequest(const ProbePoints::VMAPacketInfo &pkt_info)
{
  if(pkt_info.contextId != InvalidContextID) {
    VMAEntry *vmaEntry = table.UpdateVMAEntry(
      system->getThreadContext(pkt_info.contextId),
      pkt_info.addr, pkt_info.isRead);

    ProtoMessage::VMA vma_msg;

    vma_msg.set_tick(curTick());
    vma_msg.set_vmarea(vmaEntry->getVMArea());
    vma_msg.set_vmstart(vmaEntry->getVMStart());
    vma_msg.set_vmend(vmaEntry->getVMEnd());
    vma_msg.set_vmfile(vmaEntry->getVMFile());
    vma_msg.set_vmdentry(vmaEntry->getVMDentry());
    vma_msg.set_diname(vmaEntry->getDIname());
    vma_msg.set_rcount(vmaEntry->getRCount());
    vma_msg.set_wcount(vmaEntry->getWCount());

    traceStream->write(vma_msg);
  }
}

void
VMATableProbe::handleRequest(const ProbePoints::PacketInfo &pkt_info) {

}

VMATableProbe *
VMATableProbeParams::create()
{
    return new VMATableProbe(this);
}
