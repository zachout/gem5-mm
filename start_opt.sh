#!/bin/bash

# var="booting"
# if [[ -z "$var" ]]; then

var="bootopt"

export M5_HOME=/media/yannes/research/gem5-mm
export M5_PATH=/home/yannes/Documents/gem5-system

disk="androidmm.img"
kernel="vmlinux.full2"
dtb="vexpress-v2p-ca15-tc1-gem5.dtb"

simple_opts="--outdir=benchmarks/$var \
 --stats-file=${var}_stats_region_not_needed.txt \
 configs/example/fs.py \
 --kernel=${kernel} \
 --disk=${disk} --cpu-type=arm_detailed \
 --dtb-file=${dtb} \
 --machine-type=VExpress_EMM \
 --os-type=android-kitkat --num-cpus=1 --mem-size=1800MB" # --script=getcheckpoint.rcS"

opts="${simple_opts} \
 --caches \
 --l1d_size=32kB \
 --l1i_size=32kB \
 --l1d_assoc=2 \
 --l1i_assoc=2 \
 --l2cache --l2_size=1024kB --l2_assoc=16 \
 --cacheline_size=64 \
 --vmastats --vma-stats-file=vmastats.txt"

cmd="build/ARM/gem5.opt ${opts_debug} ${opts}"

echo -e "Running ${cmd}\n"

mkdir -p benchmarks/${var}
touch benchmarks/${var}/system.terminal
touch benchmarks/${var}/system.framebuffer.bmp

gem5monitor="gem5monitor --start -f benchmarks/${var}/system.terminal -f benchmarks/${var}/system.framebuffer.bmp"

if [ "$#" -eq 1 ]; then
    if [ "$1" == "build" ]; then
        ${gem5monitor} &
	time scons build/ARM/gem5.opt -j8 && time ${cmd}
    elif [ "$1" == "buildonly" ]; then
        echo "Only running build"
        ${gem5monitor} &	
  	time scons build/ARM/gem5.opt -j8
    fi
else
    echo "Skipping build."
    ${gem5monitor} &
    time ${cmd}
fi
