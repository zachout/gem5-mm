#!/bin/bash

if [[ "$#" -lt "1" ]]; then
    echo "Usage $0: <benchmark>"
    exit
fi
var=$1
num="1"

if [[ "$#" -eq "2" ]]; then
    num="$2"
fi

export M5_HOME=/opt/gem5-mm
export M5_PATH=/opt/gem5-system

options=("boot"
         "bootraw"
	 "booting"
	 "com.android.browser.launchfile"
	 "com.android.calculator2.add"
	 "com.android.calendar.showepoch"
	 "com.android.contacts.createcontact"
	 "com.android.deskclock.start_stopwatch"
	 "com.android.gallery3d.viewimage"
	 "com.android.gallery3d.viewvideo"
	 "com.android.gallery.viewimage"
	 "com.android.launcher3.home"
	 "com.android.launcher.home"
	 "com.android.messaging.showall"
	 "com.android.messaging.showconversation"
	 "com.android.music.playaudio"
	 "com.android.packageinstaller.install"
	 "Aard.Search"
	 "CoolReader.Run"
	 "Countdown.Run"
	 "Doom.Run"
	 "FrozenBubble.Run"
	 "JetBoy.Run"
	 "Odr.Presentation.Read"
	 "Odr.Spreadsheet.Read"
	 "Odr.Text.Read"
	 "OsmAnd.Map"
	 "OsmAnd.Navigate"
	 "Pm.Cli.Install"
	 "Pm.Install"
	 "Vlc.Music.Play"
	 "Vlc.Music.Service.Play"
	)

if [[ ! " ${options[@]} " =~ " ${var} " ]]; then
    echo "Invalid benchmark: '${var}'"
    exit 1
fi

function clean_children {
    kill -9 ${GEM5MONITOR_ID}
    exit
}

# var="bootvma" # "fastboot.gles"
disk="androidmm.03.img"
# kernel="vmlinux.mali"
kernel="vmlinux.vma"
dtb="vexpress-v2p-ca15-tc1-gem5.dtb"
script="myscripts/waitForBoot.rcS" # "myscripts/my_vmaprobe.ko"

simple_opts="--outdir=benchmarks/$var \
 --stats-file=${var}_stats.txt \
 configs/example/fs.py \
 --kernel=${kernel} \
 --disk=${disk} --cpu-type=atomic \
 --dtb-file=${dtb} \
 --machine-type=VExpress_EMM \
 --os-type=android-kitkat --num-cpus=1 --mem-size=1800MB -r ${num}" # --script=${script}" 

opts="${simple_opts} \
 --caches \
 --l1d_size=32kB \
 --l1i_size=32kB \
 --l1d_assoc=2 \
 --l1i_assoc=2 \
 --l2cache --l2_size=1024kB --l2_assoc=16 \
 --cacheline_size=64"
#  --vmastats --vma-stats-file=vmastats.txt"

cmd="build/ARM/gem5.fast ${opts_debug} ${opts}"

echo -e "Running ${cmd}\n"

if [ ! -d benchmarks/${var} ]; then
    mkdir -p benchmarks/${var}
fi

touch benchmarks/${var}/system.terminal
touch benchmarks/${var}/system.framebuffer.bmp
touch benchmarks/${var}/system.cows.txt
touch benchmarks/${var}/system.vmas.txt
touch benchmarks/${var}/system.stats.txt

gem5monitor="gem5monitor -o /tmp/gem5.docs -f benchmarks/${var}/system.terminal -f benchmarks/${var}/system.framebuffer.bmp -f benchmarks/${var}/system.cows.txt -f benchmarks/${var}/system.vmas.txt -f benchmarks/${var}/system.stats.txt"

trap clean_children SIGHUP SIGINT SIGTERM EXIT

${gem5monitor} &
GEM5MONITOR_ID=$!

time ${cmd}
