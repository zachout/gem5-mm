#!/bin/bash

# var="booting"
# if [[ -z "$var" ]]; then

var="hello"

dtb="vexpress-v2p-ca15-tc1-gem5.dtb"
kernel="vmlinux.mali"
export M5_HOME=/media/yannes/VM/gem5-mm
export M5_PATH=/media/yannes/VM/gem5-system

simple_opts="--outdir=benchmarks/$var \
 --stats-file=${var}_stats_region_not_needed.txt \
 configs/example/se.py \
 --cpu-type=arm_detailed \
 --num-cpus=1 --mem-size=1800MB \
 --cachetrace \
 -c tests/test-progs/hello/bin/arm/linux/hello" # --mem-type=SimpleMemory 

l1_opts="--l1d_size=32kB \
 --l1i_size=32kB \
 --l1d_assoc=2 \
 --l1i_assoc=2 \
 --cacheline_size=64" #  --num_regions=2"

l2_opts="--l2cache --l2_size=1024kB --l2_assoc=16"

# trace_opts="--elastic-trace-en --data-trace-file=deptrace.proto.txt  --inst-trace-file=fetchtrace.proto.txt"
# opts_debug="--debug-flags=RegionCache --debug-file=${var}_trace.txt.gz"
opts_debug="--debug-flags=CacheRepl,CacheReplacement --debug-file=${var}_trace.txt.gz"
# opts_debug="--debug-flags=XBar,RegionAll,Request --debug-file=${var}_trace.txt.gz"

opts="${simple_opts} --caches ${l1_opts} ${l2_opts}"

# cmd="gdb -batch -x gdb.run --args build/ARM/gem5.debug ${opts_debug} ${opts} ${trace_opts}"
cmd="build/ARM/gem5.opt ${opts_debug} ${opts} ${trace_opts}"

echo -e "Running ${cmd}\n"

benchDir="${M5_HOME}/benchmarks/${var}"
monitor_cmd="/home/yannes/Dropbox/Apps/python/monitor-gem5/Gem5Monitor.py -b ${benchDir}"

# touch ${benchDir}/system.framebuffer.bmp 
# touch ${benchDir}/system.terminal
# ${monitor_cmd} &

if [ "$#" -eq 1 ]; then
    if [ "$1" == "build" ]; then
	time scons build/ARM/gem5.opt -j8 && time ${cmd}
    elif [ "$1" == "buildonly" ]; then
	echo "Only running build"
	time scons build/ARM/gem5.opt -j8
    fi
else
    echo "Skipping build."
    time ${cmd}
fi
