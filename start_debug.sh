#!/bin/bash

# var="booting"
# if [[ -z "$var" ]]; then

var="regionboot"

export M5_HOME=/media/yannes/VM/gem5-mm
export M5_PATH=/media/yannes/VM/gem5-system

disk="androidmm.img"
kernel="vmlinux.mali"
dtb="vexpress-v2p-ca15-tc1-gem5.dtb"

simple_opts="--outdir=benchmarks/$var \
 --stats-file=${var}_stats_region_not_needed.txt \
 configs/example/fs.py \
 --kernel=${kernel} \
 --disk=${disk} --cpu-type=arm_detailed \
 --dtb-file=${dtb} \
 --machine-type=VExpress_EMM \
 --os-type=android-kitkat --num-cpus=1 --mem-size=1800MB \
 --script=getcheckpoint.rcS"

opts="${simple_opts} \
 --caches \
 --l1d_size=32kB \
 --l1i_size=32kB \
 --l1d_assoc=2 \
 --l1i_assoc=2 \
 --l2cache --l2_size=1024kB --l2_assoc=16 \
 --cacheline_size=64"

opts_debug="--debug-flags=RegionCache --debug-file=${var}_trace.txt.gz"
# opts_debug="--debug-flags=Exec,Cache --debug-file=${var}_trace.txt.gz"

# cmd="gdb -batch -x gdb.run --args build/ARM/gem5.debug ${opts_debug} ${opts}"
cmd="build/ARM/gem5.debug ${opts_debug} ${opts}"

echo -e "Running ${cmd}\n"

# time LD_LIBRARY_PATH=/srv/store/yannes/dev/toolchain/x86_64-unknown-linux-gnu/lib /srv/store/yannes/dev/toolchain/x86_64-unknown-linux-gnu/lib/ld-linux-x86-64.so.2 ${cmd}

touch benchmarks/${var}/system.framebuffer.bmp
touch benchmarks/${var}/system.terminal

monitor="/home/yannes/Dropbox/Apps/python/monitor-gem5/Gem5Monitor.py -b ${M5_HOME}/benchmarks/${var} -f system.framebuffer.bmp -f system.terminal"

if [ "$#" -eq 1 ]; then
    if [ "$1" == "build" ]; then
        echo "Run: ${monitor} &"
	time scons build/ARM/gem5.debug -j8 && time ${cmd}
    elif [ "$1" == "buildonly" ]; then
        echo "Only running build"
  	time scons build/ARM/gem5.debug -j8
    fi
else
    echo "Skipping build."
    echo "Run: ${monitor} &"
    time ${cmd}
fi
