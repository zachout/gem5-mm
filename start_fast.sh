#!/bin/bash

export M5_HOME=/opt/gem5-mm
export M5_PATH=/opt/gem5-system

var="booting" # "fastboot.gles"
disk="androidmm.03.img"
# kernel="vmlinux.mali"
kernel="vmlinux.vma"
dtb="vexpress-v2p-ca15-tc1-gem5.dtb"
script="myscripts/waitForBoot.rcS" # "myscripts/my_vmaprobe.ko"

simple_opts="--outdir=benchmarks/$var \
 --stats-file=${var}_stats_region_not_needed.txt \
 configs/example/fs.py \
 --kernel=${kernel} \
 --disk=${disk} --cpu-type=atomic \
 --dtb-file=${dtb} \
 --machine-type=VExpress_EMM \
 --os-type=android-kitkat --num-cpus=1 --mem-size=1800MB --script=${script}" 

opts="${simple_opts} \
 --caches \
 --l1d_size=32kB \
 --l1i_size=32kB \
 --l1d_assoc=2 \
 --l1i_assoc=2 \
 --l2cache --l2_size=1024kB --l2_assoc=16 \
 --cacheline_size=64"
#  --vmastats --vma-stats-file=vmastats.txt"

cmd="build/ARM/gem5.fast ${opts_debug} ${opts}"

echo -e "Running ${cmd}\n"

if [ "$#" -eq 1 ]; then
    if [ "$1" == "build" ]; then
	time scons build/ARM/gem5.fast -j8 && time ${cmd}
    elif [ "$1" == "buildonly" ]; then
        echo "Only running build"
  	time scons build/ARM/gem5.fast -j8
    fi
else
    echo "Skipping build."
    time ${cmd}
fi
