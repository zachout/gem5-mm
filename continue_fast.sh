#!/bin/bash

# var="booting"
# if [[ -z "$var" ]]; then

export M5_HOME=/media/yannes/VM/gem5-mm
export M5_PATH=/media/yannes/VM/gem5-system

if [ "$#" -lt 3 ]; then
    echo "Usage: $0 <benchmark> <chkpt num> <trace|notrace> [script]"
    exit 0
fi
var="$1"
checkpoint="$2"

disk="androidmm.img"
kernel="vmlinux.mali"
dtb="vexpress-v2p-ca15-tc1-gem5.dtb"

simple_opts="--outdir=benchmarks/$var \
 --stats-file=${var}_stats_region_not_needed.txt \
 configs/example/fs.py \
 --kernel=${kernel} \
 --disk=${disk} --cpu-type=arm_detailed \
 --dtb-file=${dtb} \
 --machine-type=VExpress_EMM \
 --os-type=android-kitkat --num-cpus=1 --mem-size=1800MB \
 -r ${checkpoint}"

if [ "$3" == "trace" ]; then
    simple_opts="${simple_opts} --cachetrace"
fi

if [ "$#" -eq 4 ]; then
    if [ -f "$4" ]; then
        echo "Starting with script $4"
        simple_opts="${simple_opts} --script=${4}"
    else
        echo "Script ${4} does not exist"
    fi
fi

opts="${simple_opts} \
 --caches \
 --l1d_size=32kB \
 --l1i_size=32kB \
 --l1d_assoc=2 \
 --l1i_assoc=2 \
 --l2cache --l2_size=1024kB --l2_assoc=16 \
 --cacheline_size=64"

cmd="build/ARM/gem5.fast ${opts_debug} ${opts}"

echo -e "Running ${cmd}\n"

touch benchmarks/${var}/system.framebuffer.bmp
touch benchmarks/${var}/system.terminal

monitor="/home/yannes/Dropbox/Apps/python/monitor-gem5/Gem5Monitor.py -b ${M5_HOME}/benchmarks/${var} -f system.framebuffer.bmp -f system.terminal"

echo "Skipping build."
echo "Run: ${monitor} &"
time ${cmd}
